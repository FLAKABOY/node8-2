import express from 'express';
import json from 'body-parser';
import alumnosDb from '../models/alumnos.js';

export const router = express.Router();
export default { router };


router.get('/', (req, res) => {
    res.render('index', { titulo: "Mis practicas js", nombre: "Jorge Rendon Estrada" });
});

router.get('/tabla', (req, res) => {
    const params = {
        numero: req.query.numero
    }
    res.render('tabla', params);
});

router.post('/tabla', (req, res) => {
    const params = {
        numero: req.body.numero
    }
    res.render('tabla', params);
});

router.get('/cotizacion', (req, res) => {
    const params = {
        numero: req.query.numero,
        nombre: "Jorge Rendon Estrada",
        titulo: "Mis practicas js"
    }
    res.render('cotizacion', params);
});

router.post('/cotizacion', (req, res) => {
    const valor = parseFloat(req.body.valor);
    const pInicial = parseFloat(req.body.pInicial);
    const plazos = parseInt(req.body.plazos);

    const pagoInicial = valor * (pInicial / 100);
    const cuotaMensual = ((valor - pagoInicial) * (1 - pInicial / 100)) / plazos;


    const params = {
        numero: req.body.numero,
        pagoInicial,
        cuotaMensual,
        nombre: "Jorge Rendon Estrada",
        titulo: "Mis practicas js"
    }
    res.render('cotizacion', params);
});

let rows;
router.get('/alumnos', async (req, res) => {
    rows = await alumnosDb.mostrarTodos();
    const params = {
        nombre: "Jorge Rendon Estrada",
        titulo: "Mis practicas js",
        numero: req.query.numero,
        rows
    }

    res.render('alumnos', params);
});

router.post('/alumnos', async (req, res) => {
    let params;
    try {
        params = {
            matricula: req.body.matricula,
            nombre: req.body.nombre,
            domicilio: req.body.domicilio,
            sexo: req.body.sexo,
            especialidad: req.body.especialidad
        }
        const response = await alumnosDb.insertar(params);
        // res.render('alumnos'); // No es necesario renderizar aquí

        // Asignar a rows antes de renderizar la plantilla
        const rows = await alumnosDb.mostrarTodos();

        const data = {
            titulo: "Mis practicas js",
            numero: req.query.numero,
            rows
        }
        res.render('alumnos', data);
    }
    catch (err) {
        console.log(err);
        res.status(400).send(`Sucedio un error: ${err}`);
    }
});

